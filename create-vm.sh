#!/bin/bash
eval $(cat vm.config)

multipass launch focal --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml 

if [ ! -d "config" ];then
  mkdir config
fi

echo "Initialize ${vm_name}..."

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"
echo ${IP} >> ./config/config.txt

L1='PATH=~/graalvm-ce-java11-20.2.0/bin:$PATH'
L2='export JAVA_HOME=~/graalvm-ce-java11-20.2.0'

multipass exec ${vm_name} -- bash <<EOF
curl https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh --output install.sh
chmod +x install.sh
./install.sh
rm install.sh

wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.2.0/graalvm-ce-java11-linux-amd64-20.2.0.tar.gz
tar -xvzf graalvm-ce-java11-linux-amd64-20.2.0.tar.gz

echo $L1 >> ~/.bashrc
echo $L2 >> ~/.bashrc
source ~/.bashrc

gu install ruby
gu install python
gu install wasm
EOF

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt install -y nodejs
apt install -y maven
apt install -y redis-server
EOF

multipass exec ${vm_name} -- bash <<EOF
git clone https://gitlab.com/funky-cloud/funky-wasm-runtime.git
git clone https://gitlab.com/funky-cloud/funky-kotlin-runtime.git
git clone https://gitlab.com/funky-cloud/funky-graalvm-runtime.git
git clone https://gitlab.com/funky-cloud/funky-store.git
git clone https://gitlab.com/funky-cloud/funky-regulator.git

cd ~/funky-wasm-runtime
./build.sh

cd ~/funky-kotlin-runtime
./build.sh

cd ~/funky-graalvm-runtime
./build.sh

cd ~/funky-store
npm install

cd ~/funky-regulator
./build.sh
EOF

multipass exec ${vm_name} -- bash <<EOF

cd ~/funky-store
nohup ./redis.sh &>/dev/null &
nohup ./start.sh &>/dev/null &

cd ~/funky-regulator
nohup ./start.sh &>/dev/null &

echo "token is: ${token}"
EOF
