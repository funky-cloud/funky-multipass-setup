# Setup

- `git clone` this repository
- `create-vm.sh` initializes a Multipass VM with **funKy-cloud**
- when the platform is running use the **funKy-CLI**: https://gitlab.com/funky-cloud/funky-cli to play with **funKtions**
- you'll find the IP of the VM in `./config/config.txt`

> WIP 🚧

