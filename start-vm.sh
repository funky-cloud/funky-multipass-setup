#!/bin/bash
eval $(cat vm.config)
multipass start ${vm_name}

multipass exec ${vm_name} -- bash <<EOF

cd ~/funky-store
nohup ./redis.sh &>/dev/null &
nohup ./start.sh &>/dev/null &

cd ~/funky-regulator
nohup ./start.sh &>/dev/null &

EOF
